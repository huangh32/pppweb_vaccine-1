import threading, datetime, json
import sys
from django.utils.deprecation import MiddlewareMixin
from django import http
from django.http import HttpResponseRedirect
from django.conf import settings
from django.urls.resolvers import URLResolver
from django.core.mail import mail_admins
from PhmWeb.common import EmailUtils, Utils
from PhmWeb.biz.sessionmanager import SessionManager
from PhmWeb.dbmodel.DbPhmLogAccess import DbPhmLogAccess
from PhmWeb.dbmodel.DbPhmLogServerError import DbPhmLogServerError
from somos.biz.community_service import CommunityService


class SessionValidateMiddleware(MiddlewareMixin):
    def process_request(self, request):
        meta = request.META
        pathinfo = "" + meta['PATH_INFO']

        if pathinfo.find('/sso') == 0 or pathinfo.find('/api') == 0 \
                or pathinfo.find('/dev/test') == 0 \
                or pathinfo.find('/s') == 0 \
                or pathinfo.find('/favicon.ico') == 0:
            return

        #if pathinfo == "/sso/getTicket" or pathinfo == "/sso/login": return

        params = ''
        for item in request.GET:
            params += ('&{0}={1}'.format(item, request.GET[item]))
        if params is not None and params != '':
            pathinfo += '?eeec=1' + params

        #url need to be encoded
        encode_path = Utils.base64_encode(pathinfo)

        usession = SessionManager.get_session(request)  # here will get session from cookie
        print(f'usession={usession}')
        uname = request.session.get('username', None)
        host = CommunityService(request).get_domain()
        #if (uname is None or uname == '') and host.find('somostesting') == -1:
        if (uname is None or uname == '') and host.find('somosandbp') != -1:
            if len(pathinfo) > 5:
                if 'login' not in pathinfo and 'checkSession' not in pathinfo and 'signup' not in pathinfo:
                    print('need login system: ' + pathinfo )
                    return HttpResponseRedirect("/p/login/?rtn=" + encode_path)


    def process_response(self, request, response):
        return response


# log all visit log and link to session
class AccessLogMiddleware(MiddlewareMixin):

    def process_request(self, request):
        request._start = datetime.datetime.now()
        meta = request.META
        usession = SessionManager.get_session(request)
        pathinfo = meta['PATH_INFO']
        if pathinfo.find('/sso') == 0 or pathinfo.find('/checkSession') == 0 or pathinfo.find('/p/static')==0:
            return

        log ={'OnlineId': usession.OnlineId, 'UserName': usession.UserName, 'PatientID': usession.PatientID, 'PathInfo': pathinfo}

        if 'HTTP_X_FORWARDED_FOR' in request.META:
            log['IP'] = request.META['HTTP_X_FORWARDED_FOR']
        else:
            log['IP'] = request.META['REMOTE_ADDR']

        log['VisitDate'] = datetime.datetime.now()
        log['ParamGet'] = request.GET
        log['ParamPost'] = request.POST
        log['VisitSystem'] = 'PppWeb'
        log['SystemVersion'] = 20210123
        if request.META and 'HTTP_REFERER' in request.META:
            log['HttpReferer'] = '{0}'.format(request.META['HTTP_REFERER'])

        ip = log['IP']
        # if the ip is 192.168.168.105, then ignore it because their are too many such useless log
        if ip == '127.0.0.1' or pathinfo in ['/', '/favicon.ico', '/static/images/favicon.ico']:
            return
        #elif usession.OnlineId <= 0 and pathinfo is not None and not pathinfo.startswith('/api'): return
        else:
            request._my_access_log = log

    def process_response(self, request, response):
        if hasattr(request, '_start') and hasattr(request, '_my_access_log'):
            dt_now = datetime.datetime.now()
            log = request._my_access_log
            log['VisitResponse'] = dt_now
            log['ProcessTime'] = (dt_now - request._start).total_seconds()
            DbPhmLogAccess.instance().add_access_log(log)
        return response


class CrequestMiddleware(MiddlewareMixin):
    """
    Provides storage for the "current" request object, so that code anywhere
    in your project can access it, without it having to be passed to that code
    from the view.
    """
    _requests = {}

    def process_request(self, request):
        """
        Store the current request.
        """
        self.__class__.set_request(request)

    def process_response(self, request, response):
        """
        Delete the current request to avoid leaking memory.
        """
        self.__class__.del_request()
        return response

    @classmethod
    def get_request(cls, default=None):
        """
        Retrieve the request object for the current thread, or the optionally
        provided default if there is no current request.
        """
        return cls._requests.get(threading.current_thread(), default)

    @classmethod
    def set_request(cls, request):
        """
        Save the given request into storage for the current thread.
        """
        cls._requests[threading.current_thread()] = request

    @classmethod
    def del_request(cls):
        """
        Delete the request that was stored for the current thread.
        """
        cls._requests.pop(threading.current_thread(), None)


class StandardExceptionMiddleware(MiddlewareMixin):
    def process_exception(self, request, exception):
        # Get the exception info now, in case another exception is thrown later.
        if isinstance(exception, http.Http404): # Page not found
            return self.handle_404(request, exception)
        else:
            self.handle_500(request, exception)

    def handle_404(self, request, exception):
        if settings.DEBUG:
            from django.views import debug
            return debug.technical_404_response(request, exception)
        else:
            callback, param_dict = resolver(request).resolve404()
            return callback(request, **param_dict)

    def handle_500(self, request, exception):
        exc_info = sys.exc_info()
        if settings.DEBUG:
            self.log_exception(request, exception, exc_info)
            return self.debug_500_response(request, exception, exc_info)
        else:
            self.log_exception(request, exception, exc_info)
            return self.production_500_response(request, exception, exc_info)

    def debug_500_response(self, request, exception, exc_info):
        from django.views import debug
        return debug.technical_500_response(request, *exc_info)

    def production_500_response(self, request, exception, exc_info):
        '''Return an HttpResponse that displays a friendly error message.'''
        callback, param_dict = resolver(request).resolve500()
        return callback(request, **param_dict)

    def exception_email(self, request, exc_info):
        res_dict = dict()

        subject = 'Error (%s IP): %s' % ((request.META.get('REMOTE_ADDR') in settings.INTERNAL_IPS and 'internal' or 'EXTERNAL'), request.path)
        try:
            request_repr = repr(request)
        except:
            request_repr = "Request repr() unavailable"
        message = "%s\n\n%s" % (_get_traceback(exc_info), request_repr)

        # session info
        smg = SessionManager(request)
        if smg.session is not None:
            message += '\nsession info:{0}\n  '.format(smg.session)
            res_dict['PatientID'] = smg.session.PatientID

        # query string, refer url
        get_param = request.GET
        if get_param is not None:
            param_list = []
            for key in get_param:
                param_list.append("{0}={1}".format(key, get_param[key]))
                if key.lower() == 'clinicid':
                    res_dict['ClinicID'] = Utils.to_numeric(get_param[key])
            message += '\nquery string :{0}\n  '.format('&'.join(param_list))

        # post content
        message += '\npost string :{0}\n  '.format(json.dumps(request.POST))
        # refer url
        if request.META and 'HTTP_REFERER' in request.META:
            message += '\nHTTP_REFERER: {0}\n '.format(request.META['HTTP_REFERER'])

        res_dict['Subject'] = subject
        res_dict['Message'] = message
        res_dict['ParamGet'] = request.GET
        res_dict['ParamPost'] = request.POST
        return res_dict

    def log_exception(self, request, exception, exc_info):
        msg_obj = self.exception_email(request, exc_info)
        DbPhmLogServerError.instance().add_error_log(msg_obj, request)
        try:
            EmailUtils.send_mail(msg_obj['Subject'], msg_obj['Message'])
        except:
            print('send mail error... ')
        # mail_admins(subject, message, fail_silently=True)


def resolver(request):
    """
    Returns a RegexURLResolver for the request's urlconf.

    If the request does not have a urlconf object, then the default of
    settings.ROOT_URLCONF is used.
    """
    from django.conf import settings
    urlconf = getattr(request, "urlconf", settings.ROOT_URLCONF)
    return URLResolver(r'^/', urlconf)


def _get_traceback(self, exc_info=None):
    """Helper function to return the traceback as a string"""
    import traceback
    return '\n'.join(traceback.format_exception(*(exc_info or sys.exc_info())))
