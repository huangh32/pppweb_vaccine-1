import threading
import datetime
from PhmWeb.utils.dbconfig import get_PHM_db
from PhmWeb.common import RequestUtils


class DbPhmLogServerError:
    _instance_lock = threading.Lock()

    def __init__(self):
        print('')

    @classmethod
    def instance(cls, *args, **kwargs):
        if not hasattr(DbPhmLogServerError, "_instance"):
            with DbPhmLogServerError._instance_lock:   #keep thread safe , add lock
                if not hasattr(DbPhmLogServerError, "_instance"):
                    DbPhmLogServerError._instance = DbPhmLogServerError(*args, **kwargs)
        return DbPhmLogServerError._instance

    def add_error_log(self, restlog, request):
        RequestUtils.fill_request_meta_log(request, restlog)
        zw = get_PHM_db()
        print(f'restlog={restlog}')
        zw['vac_log_ServerError'].insert(restlog)
        return None

