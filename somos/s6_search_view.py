import datetime, json, bson
from django.shortcuts import render,  redirect
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from PhmWeb.common import DateUtils, RequestUtils
from PhmWeb.biz.sessionmanager import SessionManager
from PhmWeb.utils.dbconfig import sql_ec_db
from PhmWeb.dbmodel.ic_covid_reg_questionnaire import IcCovidRegQuestionnaire
from somos.employee_slot import EmployeeSlot
from django.templatetags.static import static

@csrf_exempt
def index(request):
    page_dict = dict()
    smg = SessionManager(request)
    page_dict['Lang'] = smg.get_lang_dict()
    page_dict['BookingDay'] = DateUtils.datetime_to_short_string(datetime.datetime.now())
    page_dict['qid'] = RequestUtils.get_string(request, 'qid')
    return render(request, 'somos/search.html', page_dict)